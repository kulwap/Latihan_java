/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package batikchoris;

import java.sql.SQLException;
/**
 *
 * @author ASUS
 */
public class DatabarangController {
    
    public DatabarangController(){
        
    }
    
    public static SQLException add(Databarang databarang){
        return databarang.insert();
    }
    
    public static SQLException edit(Databarang databarang){
        return databarang.update();
    }
    
    public static SQLException distroy(Databarang databarang){
        return databarang.delete();
    }
    
    public static DatabarangTableModel getTableModel(){
        return new DatabarangTableModel(new Databarang().select());
    } 
    
    public static DatabarangTableModel getTableModel(String value){
        return new DatabarangTableModel (new Databarang().select(value)); 
    } 
        
    public static Databarang get(String KodeBarang){
        return new Databarang().get(KodeBarang);
    }
}
