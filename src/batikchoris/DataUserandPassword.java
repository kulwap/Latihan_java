/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package batikchoris;

import config.KoneksiMysql;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class DataUserandPassword {
    private Connection connection;
    private String Username, Passwd;
    private String insert= "INSERT INTO login(username, password) VALUES(?,?)";
    private String update = "UPDATE login SET username=?, password=?";
    private String delete="DELETE FROM login WHERE username=?";
    private String select= "SELECT * FROM login";
    private String get= "SELECT * FROM login WHERE username=?";
    private String search= "SELECT * FROM login WHERE username LIKE ?";
    
    public DataUserandPassword(){
        connection=KoneksiMysql.getConnection();
    } 
    
    public SQLException insert(){
        this.setConnection(KoneksiMysql.getConnection());
        PreparedStatement statement= null;
        try{
            statement= this.getConnection().prepareStatement(insert);
            statement.setString(1, this.getUsername());
            statement.setString(2, this.getPasswd());
            statement.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
            return ex;
        }finally{
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }  
    
    public SQLException update(){
        PreparedStatement statement= null;
        try{
            statement= this.getConnection().prepareStatement(update);
            statement.setString(5, this.getUsername());
            statement.setString(1, this.getPasswd());
            statement.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
            return ex;
        }finally{
            try {
                statement.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
        return null;
            
    }
    
    public SQLException delete(){
        this.setConnection(KoneksiMysql.getConnection());
        PreparedStatement statement= null;
        try{
            statement= this.getConnection().prepareStatement(delete);
            statement.setString(1, this.getUsername());
            statement.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
            return ex;
        }finally{
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public List<DataUserandPassword> select() {
         this.setConnection(KoneksiMysql.getConnection());
        PreparedStatement statement= null;
        List<DataUserandPassword> users= null;
        try {
            users= new ArrayList<DataUserandPassword>();
            statement= getConnection().prepareStatement(select);
            ResultSet resultSet= statement.executeQuery();
            while (resultSet.next()){
                DataUserandPassword user= new DataUserandPassword();
                user.setUsername(resultSet.getString("username"));
                user.setPasswd(resultSet.getString("password"));
                users.add(user);
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            try { 
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return users;
    }
    
    public DataUserandPassword get(String Username){
          this.setConnection(KoneksiMysql.getConnection());
        PreparedStatement statement= null;
        DataUserandPassword user= new DataUserandPassword();
        try {
            statement= getConnection().prepareStatement(get);
            statement.setString(1, Username.trim());
            ResultSet resultSet= statement.executeQuery();            
            while (resultSet.next()){
                user.setPasswd(resultSet.getString("password"));
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
    
    public List<DataUserandPassword> select(String value){
        PreparedStatement statement= null;
        List<DataUserandPassword> users = null;
        try{
            users= new ArrayList<DataUserandPassword>();
            statement= getConnection().prepareStatement(search);
            statement.setString(1, "%"+value.trim()+"%");
            ResultSet resultSet=statement.executeQuery();
            while (resultSet.next()){
                DataUserandPassword user = new DataUserandPassword();
                user.setUsername(resultSet.getString("username"));
                user.setPasswd(resultSet.getString("password"));
                users.add(user);
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            try {
                statement.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
        return users;
            
    }

    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * @param connection the connection to set
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * @return the Username
     */
    public String getUsername() {
        return Username;
    }

    /**
     * @param Username the Username to set
     */
    public void setUsername(String Username) {
        this.Username = Username;
    }

    /**
     * @return the Passwd
     */
    public String getPasswd() {
        return Passwd;
    }

    /**
     * @param Passwd the Passwd to set
     */
    public void setPasswd(String Passwd) {
        this.Passwd = Passwd;
    }
    
    
    
}
