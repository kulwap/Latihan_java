/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package batikchoris;

import java.sql.SQLException;
/**
 *
 * @author ASUS
 */
public class UserController {
   
    public UserController(){
        
    }
    
    public static SQLException add(DataUserandPassword user){
        return user.insert();
    }
    
    public static SQLException edit(DataUserandPassword user){
        return user.update();
    }
    
    public static SQLException distroy(DataUserandPassword user){
        return user.delete();
    }
    
    public static UserTableModel getTableModel(){
        return new UserTableModel(new DataUserandPassword().select());
    } 
    
    public static UserTableModel getTableModel(String value){
        return new UserTableModel (new DataUserandPassword().select(value)); 
    } 
        
    public static DataUserandPassword get(String Username){
        return new DataUserandPassword().get(Username);
    }
    
}
