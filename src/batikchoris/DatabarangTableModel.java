/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package batikchoris;

import java.util.List;
import javax.swing.table.AbstractTableModel;
/**
 *
 * @author ASUS
 */
public class DatabarangTableModel extends AbstractTableModel {
    
    List<Databarang> entitys;
    
    public DatabarangTableModel(List<Databarang> entitys){
        this.entitys= entitys;        
    }

    @Override
    public int getRowCount() {
        return entitys.size();        
    }

    @Override
    public int getColumnCount() {
        return 5; //jumlah kolom yang dibutuhkan
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return entitys.get(rowIndex).getKodeBarang();
            case 1:
                return entitys.get(rowIndex).getNamaBarang();
            case 2:
                return entitys.get(rowIndex).getHargaPokok();
            case 3:
                return entitys.get(rowIndex).getHargaJual();
            case 4:
                return entitys.get(rowIndex).getStok();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column){
        switch(column){
            case 0:
                return "Kd_barang";
            case 1:
                return "nama_barang";
            case 2:
                return "harga_pokok";
            case 3:
                return "harga_jual";
            case 4:
                return "stok";
            default:
                return null;
        }       
    }    
    
}
