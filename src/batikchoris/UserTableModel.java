/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package batikchoris;

import java.util.List;
import javax.swing.table.AbstractTableModel;
/**
 *
 * @author ASUS
 */
public class UserTableModel extends AbstractTableModel{
    
    List<DataUserandPassword> entitys;
    
    public UserTableModel(List<DataUserandPassword> entitys){
        this.entitys= entitys;        
    }

    @Override
    public int getRowCount() {
        return entitys.size();        
    }

    @Override
    public int getColumnCount() {
        return 2; //jumlah kolom yang dibutuhkan
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return entitys.get(rowIndex).getUsername();
            case 1:
                return entitys.get(rowIndex).getPasswd();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column){
        switch(column){
            case 0:
                return "username";
            case 1:
                return "password";
            default:
                return null;
        }       
    }    
}
