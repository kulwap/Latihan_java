/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package batikchoris;


import config.KoneksiMysql;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author ASUS
 */
public class Penjualan {
    
    private Connection connection;
    private String No, Namakasir, Tanggal, Kodebarang, Namabarang, Harga, Jumlah, Total, Totalbayar;
    private String insert= "INSERT INTO penjualan(no_nota_penj,tgl_trans,username,total_bayar) VALUES(?,?,?,?) INTO detail_penjulan (id_detail_penjualan,no_nota_penj,kd_barang, nama_barang, harga,jumlah,total) VALUES(?,?,?,?,?,?)"; 
    private String update = "UPDATE penjualan SET id_detail_penjualan=?, tgl_trans=?, username=?, total_bayar=?, kd_barang=?, nama_barang=?, harga=?, jumlah=?,total=? WHERE no_nota_penj=?";
    private String delete="DELETE FROM penjualan WHERE no_nota_penj=?";
    private String select= "SELECT * FROM penjualan";
    private String get= "SELECT * FROM penjualan WHERE no_nota_penj=?";
    private String search= "SELECT * FROM penjualan WHERE no_nota_penj LIKE ? OR tgl_trans LIKE?";
    
    
     public Penjualan(){
        connection= KoneksiMysql.getConnection();
    } 
     
      public SQLException insert(){
        this.setConnection(KoneksiMysql.getConnection());
        PreparedStatement statement= null;
        try{
            statement= this.getConnection().prepareStatement(insert);
            statement.setString(1, this.getNo());
            statement.setString(2, this.getNamakasir());
            statement.setString(3, this.getTanggal());
            statement.setString(4, this.getKodebarang());
            statement.setString(5, this.getNamabarang());
            statement.setString(6, this.getHarga());
            statement.setString(7, this.getJumlah());
            statement.setString(8, this.getTotal());
            statement.setString(9, this.getTotalbayar());
            statement.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
            return ex;
        }finally{
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }  
    
    public SQLException update(){
        PreparedStatement statement= null;
        try{
            statement= this.getConnection().prepareStatement(update);
            statement.setString(9, this.getNo());
            statement.setString(1, this.getNamakasir());
            statement.setString(2, this.getTanggal());
            statement.setString(3, this.getKodebarang());
            statement.setString(4, this.getNamabarang());
            statement.setString(5, this.getHarga());
            statement.setString(6, this.getJumlah());
            statement.setString(7, this.getTotal());
            statement.setString(8, this.getTotalbayar());
            statement.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
            return ex;
        }finally{
            try {
                statement.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
        return null;
            
    }
    
     public SQLException delete(){
        this.setConnection(KoneksiMysql.getConnection());
        PreparedStatement statement= null;
        try{
            statement= this.getConnection().prepareStatement(delete);
            statement.setString(1, this.getNo());
            statement.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
            return ex;
        }finally{
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
   
     public List<Penjualan> select() {
         this.setConnection(KoneksiMysql.getConnection());
        PreparedStatement statement= null;
        List<Penjualan> penjualans= null;
        try {
            penjualans= new ArrayList<Penjualan>();
            statement= getConnection().prepareStatement(select);
            ResultSet resultSet= statement.executeQuery();
            while (resultSet.next()){
                Penjualan penjualan= new Penjualan();
                penjualan.setNo(resultSet.getString("no_nota_penj"));
                penjualan.setNamakasir(resultSet.getString("username"));
                penjualan.setTanggal(resultSet.getString("tgl_trans"));
                penjualan.setKodebarang(resultSet.getString("kd_barang"));
                penjualan.setNamabarang(resultSet.getString("nama_barang"));
                penjualan.setHarga(resultSet.getString("harga"));
                penjualan.setJumlah(resultSet.getString("jumlah"));
                penjualan.setTotal(resultSet.getString("total"));
                penjualan.setTotalbayar(resultSet.getString("total_bayar"));
                penjualans.add(penjualan);
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            try { 
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return penjualans;
    }
    
      public Penjualan get(String Penjualan){
          this.setConnection(KoneksiMysql.getConnection());
        PreparedStatement statement= null;
        Penjualan penjualan= new Penjualan();
        try {
            statement= getConnection().prepareStatement(get);
            statement.setString(1, getNo().trim());
            ResultSet resultSet= statement.executeQuery();            
            while (resultSet.next()){
                penjualan.setNo(resultSet.getString("no_nota_penj"));
                penjualan.setNamakasir(resultSet.getString("username"));
                penjualan.setTanggal(resultSet.getString("tgl_trans"));
                penjualan.setKodebarang(resultSet.getString("kd_barang"));
                penjualan.setNamabarang(resultSet.getString("nama_barang"));
                penjualan.setHarga(resultSet.getString("harga"));
                penjualan.setJumlah(resultSet.getString("jumlah"));
                penjualan.setTotal(resultSet.getString("total"));
                penjualan.setTotalbayar(resultSet.getString("total_bayar"));
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return penjualan;
    }
      
       public List<Penjualan> select(String value){
        PreparedStatement statement= null;
        List<Penjualan> penjualans = null;
        try{
            penjualans= new ArrayList<Penjualan>();
            statement= getConnection().prepareStatement(search);
            statement.setString(1, "%"+value.trim()+"%");
            statement.setString(2, "%"+value.trim()+"%");
            ResultSet resultSet=statement.executeQuery();
            while (resultSet.next()){
                Penjualan penjualan = new Penjualan();
                penjualan.setNo(resultSet.getString("no_nota_penj"));
                penjualan.setNamakasir(resultSet.getString("username"));
                penjualan.setTanggal(resultSet.getString("tgl_trans"));
                penjualan.setKodebarang(resultSet.getString("kd_barang"));
                penjualan.setNamabarang(resultSet.getString("nama_barang"));
                penjualan.setHarga(resultSet.getString("harga"));
                penjualan.setJumlah(resultSet.getString("jumlah"));
                penjualan.setTotal(resultSet.getString("total"));
                penjualan.setTotalbayar(resultSet.getString("total_bayar"));
                penjualans.add(penjualan);
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            try {
                statement.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
        return penjualans;
            
    }

    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * @param connection the connection to set
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * @return the No
     */
    public String getNo() {
        return No;
    }

    /**
     * @param No the No to set
     */
    public void setNo(String No) {
        this.No = No;
    }

    /**
     * @return the Namakasir
     */
    public String getNamakasir() {
        return Namakasir;
    }

    /**
     * @param Namakasir the Namakasir to set
     */
    public void setNamakasir(String Namakasir) {
        this.Namakasir = Namakasir;
    }

    /**
     * @return the Tanggal
     */
    public String getTanggal() {
        return Tanggal;
    }

    /**
     * @param Tanggal the Tanggal to set
     */
    public void setTanggal(String Tanggal) {
        this.Tanggal = Tanggal;
    }

    /**
     * @return the Kodebarang
     */
    public String getKodebarang() {
        return Kodebarang;
    }

    /**
     * @param Kodebarang the Kodebarang to set
     */
    public void setKodebarang(String Kodebarang) {
        this.Kodebarang = Kodebarang;
    }

    /**
     * @return the Namabarang
     */
    public String getNamabarang() {
        return Namabarang;
    }

    /**
     * @param Namabarang the Namabarang to set
     */
    public void setNamabarang(String Namabarang) {
        this.Namabarang = Namabarang;
    }

    /**
     * @return the Harga
     */
    public String getHarga() {
        return Harga;
    }

    /**
     * @param Harga the Harga to set
     */
    public void setHarga(String Harga) {
        this.Harga = Harga;
    }

    /**
     * @return the Jumlah
     */
    public String getJumlah() {
        return Jumlah;
    }

    /**
     * @param Jumlah the Jumlah to set
     */
    public void setJumlah(String Jumlah) {
        this.Jumlah = Jumlah;
    }

    /**
     * @return the Total
     */
    public String getTotal() {
        return Total;
    }

    /**
     * @param Total the Total to set
     */
    public void setTotal(String Total) {
        this.Total = Total;
    }

    /**
     * @return the Totalbayar
     */
    public String getTotalbayar() {
        return Totalbayar;
    }

    /**
     * @param Totalbayar the Totalbayar to set
     */
    public void setTotalbayar(String Totalbayar) {
        this.Totalbayar = Totalbayar;
    }

    
       
       
}
