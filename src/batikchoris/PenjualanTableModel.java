/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package batikchoris;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ASUS
 */
public class PenjualanTableModel extends AbstractTableModel {
    
    List<Penjualan> entitys;
    
    public PenjualanTableModel(List<Penjualan> entitys){
        this.entitys= entitys;        
    }

    @Override
    public int getRowCount() {
        return entitys.size();        
    }

    @Override
    public int getColumnCount() {
        return 9; //jumlah kolom yang dibutuhkan
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return entitys.get(rowIndex).getNo();
            case 1:
                return entitys.get(rowIndex).getNamakasir();
            case 2:
                return entitys.get(rowIndex).getTanggal();
            case 3:
                return entitys.get(rowIndex).getKodebarang();
            case 4:
                return entitys.get(rowIndex).getNamabarang();
            case 5:
                return entitys.get(rowIndex).getHarga();
            case 6:
                return entitys.get(rowIndex).getJumlah();
            case 7:
                return entitys.get(rowIndex).getTotal();
            case 8:
                return entitys.get(rowIndex).getTotalbayar();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column){
        switch(column){
            case 0:
                return "no_nota_penj";
            case 1:
                return "username";
            case 2:
                return "tgl_trans";
            case 3:
                return "kd_barang";
            case 4:
                return "nama_barang";
            case 5:
                return "harga";
            case 6:
                return "jumlah";
            case 7:
                return "total";
            case 8:
                return "total_bayar";
            default:
                return null;
        }       
    }    
    
}
