/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package batikchoris;

import config.KoneksiMysql;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author ASUS
 */
public class Databarang {
    
    private Connection connection;
    private String KodeBarang, NamaBarang, HargaPokok, HargaJual, Stok;
    private String insert= "INSERT INTO barang(kd_barang, nama_barang, harga_pokok, harga_jual, stok) VALUES(?,?,?,?,?)";
    private String update = "UPDATE barang SET nama_barang=?, harga_pokok=?, harga_jual=?, stok=? WHERE kd_barang=?";
    private String delete="DELETE FROM barang WHERE kd_barang=?";
    private String select= "SELECT * FROM barang";
    private String get= "SELECT * FROM barang WHERE kd_barang=?";
    private String search= "SELECT * FROM barang WHERE kd_barang LIKE ? OR nama_barang LIKE?";
    
    
    public Databarang(){
        connection= KoneksiMysql.getConnection();
    } 
    
    public SQLException insert(){
        this.setConnection(KoneksiMysql.getConnection());
        PreparedStatement statement= null;
        try{
            statement= this.getConnection().prepareStatement(insert);
            statement.setString(1, this.getKodeBarang());
            statement.setString(2, this.getNamaBarang());
            statement.setString(3, this.getHargaPokok());
            statement.setString(4, this.getHargaJual());
            statement.setString(5, this.getStok());
            statement.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
            return ex;
        }finally{
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }  
    
    public SQLException update(){
        PreparedStatement statement= null;
        try{
            statement= this.getConnection().prepareStatement(update);
            statement.setString(5, this.getKodeBarang());
            statement.setString(1, this.getNamaBarang());
            statement.setString(2, this.getHargaPokok());
            statement.setString(3, this.getHargaJual());
            statement.setString(4, this.getStok());
            statement.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
            return ex;
        }finally{
            try {
                statement.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
        return null;
            
    }
    
    public SQLException delete(){
        this.setConnection(KoneksiMysql.getConnection());
        PreparedStatement statement= null;
        try{
            statement= this.getConnection().prepareStatement(delete);
            statement.setString(1, this.getKodeBarang());
            statement.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
            return ex;
        }finally{
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
   
     public List<Databarang> select() {
         this.setConnection(KoneksiMysql.getConnection());
        PreparedStatement statement= null;
        List<Databarang> databarangs= null;
        try {
            databarangs= new ArrayList<Databarang>();
            statement= getConnection().prepareStatement(select);
            ResultSet resultSet= statement.executeQuery();
            while (resultSet.next()){
                Databarang databarang= new Databarang();
                databarang.setKodeBarang(resultSet.getString("kd_barang"));
                databarang.setNamaBarang(resultSet.getString("nama_barang"));
                databarang.setHargaPokok(resultSet.getString("harga_pokok"));
                databarang.setHargaJual(resultSet.getString("harga_jual"));
                databarang.setStok(resultSet.getString("stok"));
                databarangs.add(databarang);
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            try { 
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return databarangs;
    }
    
      public Databarang get(String KodeBarang){
          this.setConnection(KoneksiMysql.getConnection());
        PreparedStatement statement= null;
        Databarang databarang= new Databarang();
        try {
            statement= getConnection().prepareStatement(get);
            statement.setString(1, KodeBarang.trim());
            ResultSet resultSet= statement.executeQuery();            
            while (resultSet.next()){
                databarang.setNamaBarang(resultSet.getString("nama_barang"));
                databarang.setHargaPokok(resultSet.getString("harga_pokok"));
                databarang.setHargaJual(resultSet.getString("harga_jual"));
                databarang.setStok(resultSet.getString("stok"));
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return databarang;
    }
      
       public List<Databarang> select(String value){
        PreparedStatement statement= null;
        List<Databarang> databarangs = null;
        try{
            databarangs= new ArrayList<Databarang>();
            statement= getConnection().prepareStatement(search);
            statement.setString(1, "%"+value.trim()+"%");
            statement.setString(2, "%"+value.trim()+"%");
            ResultSet resultSet=statement.executeQuery();
            while (resultSet.next()){
                Databarang databarang = new Databarang();
                databarang.setKodeBarang(resultSet.getString("kd_barang"));
                databarang.setNamaBarang(resultSet.getString("nama_barang"));
                databarang.setHargaPokok(resultSet.getString("harga_pokok"));
                databarang.setHargaJual(resultSet.getString("harga_jual"));
                databarang.setStok(resultSet.getString("stok"));
                databarangs.add(databarang);
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            try {
                statement.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
        return databarangs;
            
    }

    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * @param connection the connection to set
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * @return the KodeBarang
     */
    public String getKodeBarang() {
        return KodeBarang;
    }

    /**
     * @param KodeBarang the KodeBarang to set
     */
    public void setKodeBarang(String KodeBarang) {
        this.KodeBarang = KodeBarang;
    }

    /**
     * @return the NamaBarang
     */
    public String getNamaBarang() {
        return NamaBarang;
    }

    /**
     * @param NamaBarang the NamaBarang to set
     */
    public void setNamaBarang(String NamaBarang) {
        this.NamaBarang = NamaBarang;
    }

    /**
     * @return the HargaPokok
     */
    public String getHargaPokok() {
        return HargaPokok;
    }

    /**
     * @param HargaPokok the HargaPokok to set
     */
    public void setHargaPokok(String HargaPokok) {
        this.HargaPokok = HargaPokok;
    }

    /**
     * @return the HargaJual
     */
    public String getHargaJual() {
        return HargaJual;
    }

    /**
     * @param HargaJual the HargaJual to set
     */
    public void setHargaJual(String HargaJual) {
        this.HargaJual = HargaJual;
    }

    /**
     * @return the Stok
     */
    public String getStok() {
        return Stok;
    }

    /**
     * @param Stok the Stok to set
     */
    public void setStok(String Stok) {
        this.Stok = Stok;
    }
 
      
}



