/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package batikchoris;

import java.sql.SQLException;

/**
 *
 * @author ASUS
 */
public class PenjualanController {
    
    public PenjualanController(){
        
    }
    
    public static SQLException add(Penjualan penjualan){
        return penjualan.insert();
    }
    
    public static SQLException edit(Penjualan penjualan){
        return penjualan.update();
    }
    
    public static SQLException distroy(Penjualan penjualan){
        return penjualan.delete();
    }
    
    public static PenjualanTableModel getTableModel(){
        return new PenjualanTableModel(new Penjualan().select());
    } 
    
    public static PenjualanTableModel getTableModel(String value){
        return new PenjualanTableModel (new Penjualan().select(value)); 
    } 
        
    public static Penjualan get(String No){
        return new Penjualan().get(No);
    }
    
}
