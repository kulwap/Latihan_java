/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package config;

import java.sql.Connection;
import java.sql.DriverManager;
/**
 *
 * @author ASUS
 */
public class KoneksiMysql {
    static Connection connection;
    
    public static Connection getConnection(){
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/batikchoris","root","");
            System.out.println("Koneksi Sukses...");
        }catch(Exception ex){
            System.out.println("Koneksi Gagal...");            
            ex.printStackTrace();
        }
        return connection;
    }
    
    public static void main(String[] args){
        KoneksiMysql.getConnection();
    }
    
    
}
